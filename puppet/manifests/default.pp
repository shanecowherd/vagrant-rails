# -*- mode: ruby -*-
# vi: set ft=ruby :

###################################################################

$enable_apache          = true
  $apache_web_url       = 'capturedknowledge.com' #Emulate URL
  $apache_static        = 'public' #leave blank unless rails

$enable_mysql           = true
  $mysql_root_password  = 'foo'
  $mysql_database       = 'development'
  $mysql_user           = 'vagrant'
  $mysql_password       = 'vagrant' 
  $mysql_url_source     = 'localhost' #d.local.capturedknowledge.com
  $mysql_url_dest       = $::ipaddress #184.0.0.0

$enable_php             = true

$enable_rails           = true
  $rails_passenger      = '3.0.17'
  $rails_bundler        = '1.1.3'
  $rails_puppet         = '2.7.19'
  $rails_rails          = '3.2.3'
  $rails_ruby           = 'ruby-1.9.2-p290'

$apt_get_update         = true

###################################################################
group { "puppet":
  ensure                => "present",
}

if $enable_apache {
  include apache
  notice("Setting up apache")
  apache::vhost { 'localhost':
    port                => '80',
    docroot             => "/vagrant/$apache_web_url/$apache_static",
    configure_firewall  => false,
  }
  host { $apache_web_url:
    ip                  => $::ipaddress,
  }
}

if $enable_mysql {
  include mysql
  notice("Setting up mysql")
  class { 'mysql::server':
    config_hash         => {
      'root_password'   => $mysql_root_password,
      'bind_address'    => $::ipaddress
    }
  }
  mysql::db { $mysql_database:
    user                => $mysql_user,
    password            => $mysql_password,
    host                => '%',
    grant               => ['all'],
  } 
  host { $mysql_url_source:
    ip                  => $mysql_url_dest,
  }
}

if $enable_rails {
  notice("Setting up Ruby")
  package { "nodejs": ensure => present }
  
  #rvm::define::user {'vagrant': } 
  
  rvm::define::version { $rails_ruby:
    ensure => 'present',
    system => 'true',
  }
  rvm::define::gem { 'puppet':
    ensure       => 'present',
    ruby_version => $rails_ruby,
    gem_version  => $rails_puppet,
  }
  rvm::define::gem { 'bundler':
    ensure       => 'present',
    ruby_version => $rails_ruby,
    gem_version  => $rails_bundler,
  }
  rvm::define::gem { 'rails':
    ensure       => 'present',
    ruby_version => $rails_ruby,
    gem_version  => $rails_rails,
  }
  rvm::define::gem { 'passenger':
    ensure       => 'present',
    ruby_version => $rails_ruby,
    gem_version  => $rails_passenger,
  }


  #class {
  #  'rvm::passenger::apache':
  #    version           => $rails_passenger,
  #    ruby_version      => $rails_ruby,
  #    mininstances      => '3',
  #    maxinstancesperapp => '0',
  #    maxpoolsize       => '30',
  #    spawnmethod       => 'smart-lv2',
  #    require           => Rvm_system_ruby[$rails_ruby];
  #} 
}
    
if $apt_get_update {
  notice("Checking Apt-Get for updates")
  exec { "apt-get update":
    command             => "/usr/bin/apt-get update && touch /tmp/apt.update",
    onlyif              => "/bin/sh -c '[ ! -f /tmp/apt.update ] || /usr/bin/find /etc/apt -cnewer /tmp/apt.update | /bin/grep . > /dev/null'",
  }   
}

class php {
  package { "php5": ensure => present }
  package { "libapache2-mod-php5": ensure => present }
}
if $enable_php {
  notice("Setting up php")
  include php
}

